import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { AuthentificationService } from 'src/app/authentification/authentification.service';
import { Geolocation} from '@capacitor/core';

@Component({
  selector: 'app-inscription-livreur',
  templateUrl: './inscription-livreur.page.html',
  styleUrls: ['./inscription-livreur.page.scss'],
})
export class InscriptionLivreurPage implements OnInit {
  dataLivreur = {
    nom: '',
    prenom: '',
    numero: '',
    email: '',
    num_carte: '',
    password: '',
    password2: '',
    moyen_livraison: '',
    type: 'livreur',
    en_livraison: false,
    statut: 'inactif'
  };
  errorMessage: any;

  constructor(private authService: AuthentificationService,
              private router: Router,
              private firestore: AngularFirestore) { }

  ngOnInit() {
  }

  submit() {
    if (this.dataLivreur.password === this.dataLivreur.password2) {
      const email = this.dataLivreur.email;
      const password = this.dataLivreur.password;
      this.authService.signUpUser(email, password).then(
        () => {
          localStorage.pseudo = this.dataLivreur.nom;
          localStorage.email = this.dataLivreur.email;
          localStorage.type = 'livreur';
          this.firestore.collection('livreur').add(this.dataLivreur);
          this.firestore.collection('user').add(this.dataLivreur);
          this.router.navigate(['livreur-tabs']);
        },
        (error) => {
          this.errorMessage = error;
        }
      );
    }
  }

  async getCurrentPosition() {
    const coordinates = await Geolocation.getCurrentPosition();
    console.log('Current', coordinates);
  }

  watchPosition() {
    const wait = Geolocation.watchPosition({}, (position, err) => {
      console.log(position);

    });
  }

}
