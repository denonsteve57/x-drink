import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-livreur-profil',
  templateUrl: './livreur-profil.page.html',
  styleUrls: ['./livreur-profil.page.scss'],
})
export class LivreurProfilPage implements OnInit {
pseudo: any;
email: any;
  constructor() {
    this.pseudo = localStorage.getItem('pseudo');
    this.email = localStorage.getItem('email');

   }

  ngOnInit() {
  }

}
