import { Component, AfterViewInit } from '@angular/core';
import { Geolocation} from '@capacitor/core';
import * as L from 'leaflet';

@Component({
  selector: 'app-livreur-geolocalisation',
  templateUrl: './livreur-geolocalisation.page.html',
  styleUrls: ['./livreur-geolocalisation.page.scss'],
})
export class LivreurGeolocalisationPage implements AfterViewInit {
  map: any;
  livreur_long: number;
  livreur_lat: number;

  constructor() { }

  ngAfterViewInit() {
    this.createMap();
  }

  createMap() {
    const cotonou = {
      lat: 6.3724685,
      lng: 2.326137
    };

    const zoom = 14;

    this.map = L.map('map' , {
      center: [cotonou.lat, cotonou.lng],
      // tslint:disable-next-line:object-literal-shorthand
      zoom: zoom
    });

    const mainLayer = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      minZoom: 14,
      maxZoom: 20,
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    });

    mainLayer.addTo(this.map);

  }

  async getPosition() {
    const position = await Geolocation.getCurrentPosition();
    console.log(position.coords.latitude, position.coords.longitude);
    this.livreur_lat = position.coords.latitude;
    this.livreur_long = position.coords.longitude;
  }
  

  watchPosition() {
    const wait = Geolocation.watchPosition({}, (position, err) => {
      this.livreur_lat = position.coords.latitude;
      this.livreur_long = position.coords.longitude;
    });
    console.log(this.livreur_lat, this.livreur_long);
  }

}
