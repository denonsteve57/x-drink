import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AlertController } from '@ionic/angular';
import { Plugins } from '@capacitor/core';
import { Geolocation} from '@capacitor/core';
import { Router } from '@angular/router';


const { LocalNotifications } = Plugins;

const { App, BackgroundTask } = Plugins;

@Component({
  selector: 'app-livreur-acceuil',
  templateUrl: './livreur-acceuil.page.html',
  styleUrls: ['./livreur-acceuil.page.scss'],
})
export class LivreurAcceuilPage implements OnInit {

  slideOptsPub = {
    autoplay: true,
    zoom: false,
    slidesPerView: 1,
    spaceBetween: 1,
  };

  codes_commandes_doublon: any[];
  commandes: any[];
  livreur_lat: number;
  livreur_long: number;
  distance: number;
  distances = [];
  distanceMin: number;
  en_livraison: boolean;
  numero_cmd = '';
  date_heure: any;
  vendeur_choisi: any;
  lat_vendeur: any;
  long_vendeur: any;
  email_acheteur: any;
  long_acheteur: any;
  lat_acheteur: any;
  codeSecret: any;
  // tslint:disable-next-line:variable-name
  detail1_client: any;
  // tslint:disable-next-line:variable-name
  detail2_client: any;
  localisation_detail1: any;
  localisation_detail2: any;
  constructor(public alertController: AlertController,
              private router: Router,
              public firestore: AngularFirestore,
            ) { }

  ngOnInit() {



   /* this.getPosition();


      // tslint:disable-next-line:prefer-const
    let email = localStorage.getItem('email');
    console.log(email);
    this.firestore.firestore.collection('livreur').where('email', '==', email).onSnapshot(
     (querySnapshot) => {
       querySnapshot.forEach((doc) => {
         console.log(doc.data().statut);
         if (doc.data().statut  === 'inactif') {
           this.en_livraison = false;
           this.getPosition();
           this.essai();
           this.notificationLocal();
         }

         if (doc.data().statut === 'actif') {
           this.en_livraison = true;
         }
       });
     }
   );*/


    App.addListener('appStateChange', (state) => {

      if (!state.isActive) {
        // The app has become inactive. We should check if we have some work left to do, and, if so,
        // execute a background task that will allow us to finish that work before the OS
        // suspends or terminates our app:

        // tslint:disable-next-line:prefer-const
        let taskId = BackgroundTask.beforeExit(async () => {
          // In this function We might finish an upload, let a network request
          // finish, persist some data, or perform some other task

          // Example of long task
           // tslint:disable-next-line:prefer-const
       let email = localStorage.getItem('email');
       console.log(email);
       this.firestore.firestore.collection('livreur').where('email', '==', email).onSnapshot(
      (querySnapshot) => {
        querySnapshot.forEach((doc) => {
          console.log(doc.data().statut);
          if (doc.data().statut  === 'inactif') {
            this.en_livraison = false;
            this.getPosition();
            this.essai();
            this.notificationLocal();
          }

          if (doc.data().statut === 'actif') {
            this.en_livraison = true;
          }
        });
      }
    );

          // Must call in order to end our task otherwise
          // we risk our app being terminated, and possibly
          // being labeled as impacting battery life
       BackgroundTask.finish({
            taskId
          });
        });
      }
    });



  }

  async presentAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      message: 'Vous devriez aller prendre la commande chez le vendeur le plus proche.',
      buttons: ['Aller chez le vendeur']
    });

    await alert.present();
    this.router.navigate(['livreur-tabs/geolocalisation']);
  }


   essai() {
    this.firestore.firestore.collection('commandes').where('statut', '==', 'en_attente' ).onSnapshot(
      (querySnapshot) => {
        this.codes_commandes_doublon = [];
        querySnapshot.forEach((doc) => {
          console.log(doc.id);
          console.log(doc.data().vendeur_choisi);

          this.codes_commandes_doublon.push(doc.data().cmdCode);
         });

        console.log(this.codes_commandes_doublon);
        this.commandes = Array.from(new Set(this.codes_commandes_doublon));
        console.log(this.commandes);

        this.commandes.forEach(commandesx => {
           this.firestore.firestore.collection('commandes').where('cmdCode', '==', commandesx).onSnapshot(
             // tslint:disable-next-line:no-shadowed-variable
             (querySnapshot) => {
               let vendeur = '';
               querySnapshot.forEach((doc) => {
                 vendeur = doc.data().vendeur_choisi;
               });
               console.log(vendeur);

               this.firestore.firestore.collection(vendeur).where('nom_depot', '==', vendeur).onSnapshot(
                   // tslint:disable-next-line:no-shadowed-variable
                   (querySnapshot) => {
                    const lat1 = this.livreur_lat;
                    const long1 = this.livreur_long;
                    console.log(lat1, long1);
                    let lat2 = 0;
                    let long2 = 0;
                    querySnapshot.forEach((doc) => {
                      console.log(doc.data().latitude, doc.data().longitude);
                      lat2 = doc.data().latitude;
                      long2 = doc.data().longitude;
                     });

                    console.log(lat2, long2);

                    this.distance = this.getDistance([lat1, long1], [lat2, long2]);
                    console.log(this.distance);

 // creation d'une collection pour chaque commande avec des documents ayant pour seul champ la distance entre les livreur et le vendeur
 // on recupere ensuite chaque distance pour  trouver la distance minimal
 // on compare la distance minimal a la distance du livreur en session
 // si les deux distances son egal on affecte la commande au livreur en session
                    this.firestore.collection(commandesx).add({distance: this.distance});

                    this.firestore.collection(commandesx).snapshotChanges()
                     .subscribe(actions => {
                       this.distances = [];
                       actions.forEach(action => {
                         // tslint:disable-next-line:no-string-literal
                         this.distances.push(action.payload.doc.data()['distance']);
                         console.log(this.distances);
                       });
                       this.distanceMin = Math.min(...this.distances);
                       console.log(this.distanceMin);
// c'est ici que tout commence
                       // changement du statut du livreur choisi (inactif => actif)
                       if (this.distanceMin === this.distance) {

                         const email = localStorage.getItem('email');

                         this.firestore.firestore.collection('livreur').where('email', '==', email).onSnapshot(
                           // tslint:disable-next-line:no-shadowed-variable
                           (querySnapshot) => {
                             querySnapshot.forEach((doc) => {
                               this.firestore.collection('livreur').doc(doc.id).update({
                                 statut: 'actif'
                               });
                             });
                           }
                         );
                         // changement de la vue grace au ngif
                         this.en_livraison = true;

                         // changement du statut de le commande(en_attente => en_livraison)
                         // pour eviter qu'elle soit encore (en_attente) et donc à la recherche d'un livreur
                         // on attribut egalement le livreur a la commande
                         this.firestore.firestore.collection('commandes').where('cmdCode', '==', commandesx).onSnapshot(
                           // tslint:disable-next-line:no-shadowed-variable
                           (querySnapshot) => {
                             // tslint:disable-next-line:variable-name
                             let email_acheteur: any;
                             // tslint:disable-next-line:variable-name
                             let vendeur_choisi: any;
                             let numero: string;
                             let horodatage: any;
                             let codeSecret: any;
                             let detail1: any;
                             let detail2: any;
                             querySnapshot.forEach((doc) => {
                               this.firestore.collection('commandes').doc(doc.id).update({
                                 statut: 'en_livraison' ,
                                 email_livreur_choisi: email
                               });

                               numero = doc.data().cmdCode;
                               console.log(this.numero_cmd );
                               horodatage = doc.data().date_livraison;
                               email_acheteur = doc.data().email_acheteur;
                               vendeur_choisi = doc.data().vendeur_choisi;
                               codeSecret = doc.data().codeSecret;
                               detail1 = doc.data().livraison_detail1;
                               detail2 = doc.data().livraison_detail2;

                             });
                             // affichage de la commande au livreur
                             this.numero_cmd = numero;
                             this.date_heure = horodatage;
                             this.codeSecret = codeSecret;
                             this.detail1_client = detail1;
                             this.detail2_client = detail2;

                             // recuperation de la localisation du vendeur et de l'acheteur

                             // cas du vendeur
                             this.vendeur_choisi = vendeur_choisi;
                             // tslint:disable-next-line:max-line-length
                             this.firestore.firestore.collection(this.vendeur_choisi).where('nom_depot', '==', this.vendeur_choisi).onSnapshot(
                               // tslint:disable-next-line:no-shadowed-variable
                               (querySnapshot) => {
                                 // tslint:disable-next-line:variable-name
                                 let lat_vendeur: any;
                                 // tslint:disable-next-line:variable-name
                                 let long_vendeur: any;
                                 // tslint:disable-next-line:variable-name
                                 let loca_detail1: any;
                                 // tslint:disable-next-line:variable-name
                                 let loca_detail2: any;
                                 querySnapshot.forEach((doc) => {
                                    lat_vendeur = doc.data().latitude;
                                    long_vendeur = doc.data().longitude;
                                    loca_detail1 = doc.data().localisation_detail1;
                                    loca_detail2 = doc.data().localisation_detail2;
                                  });

                                 this.lat_vendeur = lat_vendeur;
                                 this.long_vendeur  = long_vendeur;
                                 this.localisation_detail1 = loca_detail1;
                                 this.localisation_detail2 = loca_detail2;

                                 localStorage.lat_vendeur = this.lat_vendeur;
                                 localStorage.long_vendeur = this.long_vendeur;
                               }
                             );

                             // cas de l'acheteur
                             this.email_acheteur = email_acheteur;
                             this.firestore.firestore.collection('user').where('email', '==', this.email_acheteur).onSnapshot(
                               // tslint:disable-next-line:no-shadowed-variable
                               (querySnapshot) => {
                                 // tslint:disable-next-line:variable-name
                                 let lat_acheteur: any;
                                 // tslint:disable-next-line:variable-name
                                 let long_acheteur: any;
                                 querySnapshot.forEach((doc) => {
                                  lat_acheteur = doc.data().latitude;
                                  long_acheteur = doc.data().longitude;
                                 });
                                 this.lat_acheteur = lat_acheteur;
                                 this.long_acheteur  = long_acheteur;

                                 localStorage.lat_acheteur = this.lat_acheteur;
                                 localStorage.long_acheteur = this.long_acheteur;
                               }
                             );


                           }
                         );





                       }
                     });

                   }
                 );
             }
           );
         });
     });


  }



  getDistance(origin: any[], destination: any[]) {
    // return distance in meters
    // tslint:disable-next-line:prefer-const
    let lon1 = this.toRadian(origin[1]);
    // tslint:disable-next-line:prefer-const
    let lat1 = this.toRadian(origin[0]);
    // tslint:disable-next-line:prefer-const
    let lon2 = this.toRadian(destination[1]);
    // tslint:disable-next-line:prefer-const
    let lat2 = this.toRadian(destination[0]);

    // tslint:disable-next-line:prefer-const
    let deltaLat = lat2 - lat1;
    // tslint:disable-next-line:prefer-const
    let deltaLon = lon2 - lon1;

    // tslint:disable-next-line:prefer-const
    let a = Math.pow(Math.sin(deltaLat / 2), 2) + Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin(deltaLon / 2), 2);
    // tslint:disable-next-line:prefer-const
    let c = 2 * Math.asin(Math.sqrt(a));
    // tslint:disable-next-line:prefer-const
    let EARTH_RADIUS = 6371;
    return c * EARTH_RADIUS * 1000;
  }

  toRadian(degree: number) {
    return degree * Math.PI / 180;
  }


  async getPosition() {
    const position = await Geolocation.getCurrentPosition();
    console.log(position.coords.latitude, position.coords.longitude);
    this.livreur_lat = position.coords.latitude;
    this.livreur_long = position.coords.longitude;
  }

  async notificationLocal() {
    const notifs = await LocalNotifications.schedule({
      notifications: [
        {
          title: 'nouvel livraison',
          body: 'veuillez consulter votre page acceuil pour plus de detail',
          id: 1,
          schedule: { at: new Date(Date.now() + 1000 * 5) },
          sound: null,
          attachments: null,
          actionTypeId: '',
          extra: null
        }
      ]
    });
    console.log('scheduled notifications', notifs);
  }


}
