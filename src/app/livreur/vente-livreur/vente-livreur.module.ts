import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VenteLivreurPageRoutingModule } from './vente-livreur-routing.module';

import { VenteLivreurPage } from './vente-livreur.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VenteLivreurPageRoutingModule
  ],
  declarations: [VenteLivreurPage]
})
export class VenteLivreurPageModule {}
