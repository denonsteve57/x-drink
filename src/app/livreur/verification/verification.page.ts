import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-verification',
  templateUrl: './verification.page.html',
  styleUrls: ['./verification.page.scss'],
})
export class VerificationPage implements OnInit {

  constructor(public alertController: AlertController) { }

  ngOnInit() {
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      message: 'Il est temps de proceder à la livraison. Arrivez chez le client n\'ounliez pas scanner le code QR dans le menu.',
      buttons: ['Demarrer la livraison']
    });

    await alert.present();
  }
}
