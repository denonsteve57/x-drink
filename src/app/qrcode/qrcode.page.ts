import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-qrcode',
  templateUrl: './qrcode.page.html',
  styleUrls: ['./qrcode.page.scss'],
})
export class QrcodePage implements OnInit {
  public textTocode: string;
  public myAngularxQrCode: string = null;

  constructor() { }

  ngOnInit() {

    this.myAngularxQrCode = localStorage.getItem('qrcode');

  }

}
