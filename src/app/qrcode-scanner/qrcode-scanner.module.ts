import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { QrcodeScannerPageRoutingModule } from './qrcode-scanner-routing.module';

import { QrcodeScannerPage } from './qrcode-scanner.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    QrcodeScannerPageRoutingModule
  ],
  declarations: [QrcodeScannerPage]
})
export class QrcodeScannerPageModule {}
