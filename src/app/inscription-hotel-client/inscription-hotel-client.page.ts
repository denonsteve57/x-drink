import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Geolocation} from '@capacitor/core';
import { AuthentificationService } from '../authentification/authentification.service';

@Component({
  selector: 'app-inscription-hotel-client',
  templateUrl: './inscription-hotel-client.page.html',
  styleUrls: ['./inscription-hotel-client.page.scss'],
})
export class InscriptionHotelClientPage implements OnInit {

  dataHotel = {
    nom_hotel: '',
    nom: '',
    telephone_gerant: '',
    email: '',
    ifu: '',
    adresse_livraison: '',
    password: '',
    password2: '',
    type: 'client',
    latitude: 0,
    longitude: 0
  };
  errorMessage: any;

  constructor(private authService: AuthentificationService,
              private router: Router,
              public alertController: AlertController,
              private firestore: AngularFirestore) { }

  ngOnInit() {
  }

  soumettre() {
    if (this.dataHotel.password === this.dataHotel.password2) {
      const email = this.dataHotel.email;
      const password = this.dataHotel.password;
      this.authService.signUpUser(email, password).then(
        () => {
          localStorage.pseudo = this.dataHotel.nom;
          localStorage.email = this.dataHotel.email;
          localStorage.type = 'client';
          this.firestore.collection('hotel').add(this.dataHotel);
          this.firestore.collection('user').add(this.dataHotel);
          localStorage.latitude = this.dataHotel.latitude;
          localStorage.longitude = this.dataHotel.longitude;
          localStorage.nom_hotel = this.dataHotel.nom_hotel;
          this.router.navigate(['tabs']);
        },
        (error) => {
          this.errorMessage = error;
        }
      );
    }
  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'IMPORTANT!',
      message: ' <strong>Avant de confirmer la géolocalisation vous devez être dans votre Hotel</strong>!!!',
      buttons: [
        {
          text: 'Non',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Confirmer position',
          handler: () => {
            console.log('Confirm Okay');
            this.getPosition();
          }
        }
      ]
    });

    await alert.present();
  }

  async getPosition() {
    const position = await Geolocation.getCurrentPosition();
    this.dataHotel.latitude = position.coords.latitude;
    this.dataHotel.longitude = position.coords.longitude;
    console.log(this.dataHotel.latitude, this.dataHotel.longitude);
  }


}
