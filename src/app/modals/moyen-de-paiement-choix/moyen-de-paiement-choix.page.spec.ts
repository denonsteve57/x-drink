import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MoyenDePaiementChoixPage } from './moyen-de-paiement-choix.page';

describe('MoyenDePaiementChoixPage', () => {
  let component: MoyenDePaiementChoixPage;
  let fixture: ComponentFixture<MoyenDePaiementChoixPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MoyenDePaiementChoixPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MoyenDePaiementChoixPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
