import { Component, OnInit, AfterViewInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tutoriel',
  templateUrl: './tutoriel.page.html',
  styleUrls: ['./tutoriel.page.scss'],
})
export class TutorielPage implements OnInit , AfterViewInit {
  userId: string;
  email: string;

  constructor( public afAuth: AngularFireAuth,
               public router: Router,
               public firestore: AngularFirestore) {}

  ngAfterViewInit() {}

  ngOnInit() {
    this.afAuth.authState.subscribe(auth => {
      if (!auth) {
        console.log('non connecter');
      }else {
        console.log('connecter');
        this.userId = auth.uid;
        this.email = auth.email;
        console.log(this.userId, this.email);
        this.firestore.firestore.collection('user').where('email', '==', this.email).onSnapshot(
          (querySnapshot) => {
            querySnapshot.forEach((doc) => {
              console.log(doc.data().type);
              if (doc.data().type === 'vendeur') {
                this.router.navigate(['vendeur-tabs']);
              } else if (doc.data().type === 'client') {
                this.router.navigate(['tabs']);
              }else if (doc.data().type === 'livreur') {
                this.router.navigate(['livreur-tabs']);
              }
            });
          }
        );
      }
    });
  }

}
