import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthentificationService } from './authentification/authentification.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit  {
  client: boolean;
  vendeur: boolean;
  livreur: boolean;
  side: string;
  userType = '';
  statut: string;
  pseudo: string;
  email: string;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private authService: AuthentificationService
  ) {
    this.initializeApp();
  }


  ngOnInit() {
  }

  onMenuOpen() {
    this.pseudo = localStorage.getItem('pseudo');
    this.email = localStorage.getItem('email');
    this.userType = localStorage.getItem('user-type');
    this.statut = localStorage.getItem('type');
    console.log(this.statut);
  }


  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  deconnexion() {
    this.authService.signOut();
    this.router.navigate(['connexion-client']);
    localStorage.removeItem('pseudo');
    localStorage.removeItem('email');
    localStorage.removeItem('user-type');
    localStorage.removeItem('type');
    localStorage.removeItem('nom_dep');
    localStorage.removeItem('latitude');
    localStorage.removeItem('longitude');
    localStorage.removeItem('cart');
    localStorage.removeItem('stock_vendeur');
    localStorage.removeItem('nom_distributeur');
  }
}
