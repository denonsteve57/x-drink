import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { ModalController, AlertController } from '@ionic/angular';
import { LivraisonChoixPage } from '../modals/livraison-choix/livraison-choix.page';
import { MoyenDePaiementChoixPage } from '../modals/moyen-de-paiement-choix/moyen-de-paiement-choix.page';

@Component({
  selector: 'app-livraison-client',
  templateUrl: './livraison-client.page.html',
  styleUrls: ['./livraison-client.page.scss'],
})
export class LivraisonClientPage implements OnInit {
  statut: any;

  dataCommande = {
    statut: 'enregistrer',
    cmdCode: '',
    prix_total: 0,
    date_livraison: '',
    heure_livraison: '',
    frais_livraison: 0,
    frais_total: 0,
    produit: '',
    qty: 0,
    amount: 0,
    vendeur_choisi: '',
    moyen_livraison: '',
    email_acheteur: '',
    codeSecret: '',
    livraison_detail1: '',
    livraison_detail2: ''
   };

   qtyTotal: 0;

  livraisonAddress = {
    localisation: '',
    detail: ''
  };

  cart: any[];
  total = 0;
  // tslint:disable-next-line:variable-name
  moyen_de_paiement: any;
  constructor(private modalController: ModalController,
              private alertController: AlertController,
              private firestore: AngularFirestore,
              private router: Router) {
    this.statut = localStorage.getItem('type');
   }

  ngOnInit() {
  }
  ionViewWillEnter(){
    this.cart = JSON.parse(localStorage.getItem('cart2')) || [];
    console.log('this.cart :>> ', this.cart);
    this.cart.forEach(element => {
      if (!element.amount){
        element.amount = 250;
      }
      if (!element.qty){
        element.qty = 1;
      }
    });
    this.total = 0;
    this.qtyTotal = 0;
    this.cart.forEach(element => {
      this.total = this.total + element.qty * element.amount;
      this.dataCommande.prix_total = this.total;
      this.dataCommande.frais_livraison = 0;
      this.dataCommande.frais_total = this.total + this.dataCommande.frais_livraison;
      this.qtyTotal = this.qtyTotal + element.qty;
    });
    console.log (this.qtyTotal);

    if (this.qtyTotal <= 3) {
      this.dataCommande.frais_livraison = 350;
      this.dataCommande.frais_total = this.total + this.dataCommande.frais_livraison;
      this.dataCommande.moyen_livraison = '2Roues';
    }
    if (this.qtyTotal > 4 && this.qtyTotal < 10) {
      this.dataCommande.frais_livraison = 700;
      this.dataCommande.frais_total = this.total + this.dataCommande.frais_livraison;
      this.dataCommande.moyen_livraison = '3Roues ';
    }
  }


  livraison(){
    console.log('livraison');
    this.presentModal();
  }


  paiement(){
    console.log('paiement');
    this.presentModalPaiement();
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: LivraisonChoixPage,
      cssClass: 'livraison-modal',
      componentProps: {
      }
    });
    await modal.present();
    const { data } = await modal.onWillDismiss();
    this.livraisonAddress = data.livraison;
    console.log(data);
  }
  async presentModalPaiement() {
    const modal = await this.modalController.create({
      component: MoyenDePaiementChoixPage,
      cssClass: 'livraison-modal',
      componentProps: {
      }
    });
    await modal.present();
    const { data } = await modal.onWillDismiss();
    this.moyen_de_paiement = data.moyen_de_paiement;
    console.log(data);
  }

  commander(){
    this.dataCommande.livraison_detail1 = localStorage.getItem('livraison_localisation');
    this.dataCommande.livraison_detail2 = localStorage.getItem('livraison_detail');
    console.log(this.dataCommande.livraison_detail1, this.dataCommande.livraison_detail2);
    if (this.statut === 'client') {
      this.dataCommande.codeSecret = this.getRandomIntInclusive(100000, 900000);
      localStorage.qrcode = this.dataCommande.codeSecret;
      this.dataCommande.cmdCode = this.uuidv4();
      console.log(this.dataCommande.cmdCode);
      this.cart.forEach(element => {
        this.dataCommande.produit = element.name;
        this.dataCommande.qty = element.qty;
        this.dataCommande.amount = element.amount;
        this.dataCommande.vendeur_choisi = localStorage.getItem('depotFinal');
        this.dataCommande.email_acheteur = localStorage.getItem('email');
        console.log(this.dataCommande.vendeur_choisi);
        this.firestore.collection('commandes').add(this.dataCommande);
      });
      this.presentAlert();
      this.router.navigate(['tabs/accueil']);
      }

    if (this.statut === 'vendeur'){
      this.dataCommande.codeSecret = this.getRandomIntInclusive(100000, 900000);
      localStorage.qrcode = this.dataCommande.codeSecret;
      this.dataCommande.cmdCode = this.uuidv4();
      console.log(this.dataCommande.cmdCode);
      this.cart.forEach(element => {
        this.dataCommande.produit = element.name;
        this.dataCommande.qty = element.qty;
        this.dataCommande.amount = element.amount;
        this.dataCommande.vendeur_choisi = localStorage.getItem('depotFinal');
        this.dataCommande.email_acheteur = localStorage.getItem('email');
        console.log(this.dataCommande.vendeur_choisi);
        this.firestore.collection('commandes').add(this.dataCommande);
      });
      this.presentAlert();
      this.router.navigate(['vendeur-tabs/accueil']);
    }

  }

  async presentAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Commande effectuée',
      message: 'Votre commande a été enregistré.',
      buttons: ['OK']
    });

    await alert.present();


  }

  uuidv4() {
    // tslint:disable-next-line:only-arrow-functions
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      // tslint:disable-next-line:one-variable-per-declaration
      const r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }

  // On renvoie un entier aléatoire entre une valeur min (incluse)
// et une valeur max (incluse).
// Attention : si on utilisait Math.round(), on aurait une distribution
// non uniforme !
 getRandomIntInclusive(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}





}
