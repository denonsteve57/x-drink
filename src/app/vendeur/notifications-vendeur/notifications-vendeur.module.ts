import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NotificationsVendeurPageRoutingModule } from './notifications-vendeur-routing.module';

import { NotificationsVendeurPage } from './notifications-vendeur.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NotificationsVendeurPageRoutingModule
  ],
  declarations: [NotificationsVendeurPage]
})
export class NotificationsVendeurPageModule {}
