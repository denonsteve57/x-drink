import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Plugins } from '@capacitor/core';


const { LocalNotifications } = Plugins;

const { App, BackgroundTask } = Plugins;


@Component({
  selector: 'app-vendeur-accueil',
  templateUrl: './vendeur-accueil.page.html',
  styleUrls: ['./vendeur-accueil.page.scss'],
})
export class VendeurAccueilPage implements OnInit {
  commande = false;
  vendeur_actif = '';
  commandes = [];
  codes_commandes_doublon = [];
  option_commande = 'ACCEPTER'
  constructor( private firestore: AngularFirestore) {

   }

   ngOnInit() {
      // Afficher la commande au vendeur choisi

      this.vendeur_actif = localStorage.getItem('stock_vendeur');
      this.firestore.firestore.collection('commandes').where('statut', '==', 'enregistrer' ).onSnapshot(
        (querySnapshot) => {
          this.codes_commandes_doublon = [];
          querySnapshot.forEach((doc) => {
            console.log(doc.id);
            console.log(doc.data().vendeur_choisi);

            this.codes_commandes_doublon.push(doc.data().cmdCode);

            if (doc.data().vendeur_choisi === this.vendeur_actif ) {
               this.commande = true ;


               App.addListener('appStateChange', (state) => {

                if (!state.isActive) {
                  // The app has become inactive. We should check if we have some work left to do, and, if so,
                  // execute a background task that will allow us to finish that work before the OS
                  // suspends or terminates our app:

                  // tslint:disable-next-line:prefer-const
                  let taskId = BackgroundTask.beforeExit(async () => {
                    // In this function We might finish an upload, let a network request
                    // finish, persist some data, or perform some other task

                    // Example of long task

                    this.notificationLocal();

                    // Must call in order to end our task otherwise
                    // we risk our app being terminated, and possibly
                    // being labeled as impacting battery life
                    BackgroundTask.finish({
                      taskId
                    });
                  });
                }
              });
             }
           });


          console.log(this.codes_commandes_doublon);
          this.commandes = Array.from(new Set(this.codes_commandes_doublon));
          console.log(this.commandes);
       });
   }


   option_commande_change (item) {

// changer le statut de la commnade après acceptation par le vendeur (en attente)

      this.commandes.forEach(commandesx => {
        if (commandesx === item) {
          this.firestore.firestore.collection('commandes').where('cmdCode', '==', commandesx).onSnapshot(
            (querySnapshot) => {
              querySnapshot.forEach((doc) => {
                this.firestore.collection('commandes').doc(doc.id).update({
                  statut: 'en_attente'
                });
              });
            }
          );
        }
      });




   }


   async notificationLocal() {
    const notifs = await LocalNotifications.schedule({
      notifications: [
        {
          title: 'nouvel commande',
          body: 'veuillez consulter votre page acceuil pour plus de detail',
          id: 1,
          schedule: { at: new Date(Date.now() + 1000 * 5) },
          sound: null,
          attachments: null,
          actionTypeId: "",
          extra: null
        }
      ]
    });
    console.log('scheduled notifications', notifs);
  }




}
