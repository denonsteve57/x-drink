import { Component, AfterViewInit } from '@angular/core';
import * as L from 'leaflet';

@Component({
  selector: 'app-adresse-vendeur',
  templateUrl: './adresse-vendeur.page.html',
  styleUrls: ['./adresse-vendeur.page.scss'],
})
export class AdresseVendeurPage implements AfterViewInit {
  map: any;

  // tslint:disable-next-line:variable-name
  nom_dep: any;

  latitude: any;

  longitude: any;

  smallIcon = new L.Icon({
    iconUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-icon.png',
    iconRetinaUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-icon-2x.png',
    iconSize:    [25, 41],
    iconAnchor:  [12, 41],
    popupAnchor: [1, -34],
    shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
    shadowSize:  [41, 41]
  });
  // tslint:disable-next-line:variable-name
  nom_hotel: string;
  // tslint:disable-next-line:variable-name
  nom_bar: string;

  constructor() {
    this.nom_hotel = localStorage.getItem('nom_hotel')
    this.nom_bar = localStorage.getItem('nom_bar');
    this.nom_dep = localStorage.getItem('nom_dep');
    this.latitude = localStorage.getItem('latitude');
    this.longitude = localStorage.getItem('longitude');

    console.log(this.nom_dep, this.latitude, this.longitude);
   }

  ngAfterViewInit() {

    this.createMap();
  }

  createMap() {
    /*const cotonou = {
      lat: 6.3724685,
      lng: 2.326137
    };*/

    const position = {
      lat: this.latitude,
      lng: this.longitude
    };

    const zoom = 17;

    this.map = L.map('map' , {
      center: [position.lat, position.lng],
      // tslint:disable-next-line:object-literal-shorthand
      zoom: zoom
    });

    const mainLayer = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      minZoom: 14,
      maxZoom: 20,
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    });

    mainLayer.addTo(this.map);
    this.addMarker(position);
  }

  addMarker(coords) {
    const marker = L.marker([coords.lat, coords.lng], {icon: this.smallIcon});
    marker.addTo(this.map);

  }


}
