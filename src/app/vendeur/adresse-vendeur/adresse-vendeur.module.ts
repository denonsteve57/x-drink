import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AdresseVendeurPageRoutingModule } from './adresse-vendeur-routing.module';

import { AdresseVendeurPage } from './adresse-vendeur.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AdresseVendeurPageRoutingModule
  ],
  declarations: [AdresseVendeurPage]
})
export class AdresseVendeurPageModule {}
