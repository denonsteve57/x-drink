import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdresseVendeurPage } from './adresse-vendeur.page';

const routes: Routes = [
  {
    path: '',
    component: AdresseVendeurPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdresseVendeurPageRoutingModule {}
