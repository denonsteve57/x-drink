import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddDepotPage } from './add-depot.page';

const routes: Routes = [
  {
    path: '',
    component: AddDepotPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddDepotPageRoutingModule {}
