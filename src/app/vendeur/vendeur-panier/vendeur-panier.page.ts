import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-vendeur-panier',
  templateUrl: './vendeur-panier.page.html',
  styleUrls: ['./vendeur-panier.page.scss'],
})
export class VendeurPanierPage implements OnInit {
  // tslint:disable-next-line:variable-name
  code_commande: any;
  // tslint:disable-next-line:variable-name
  vendeur_actif: string;
  produits = [];
  numero_commande: any;
  total: any;
  nom_livreur: any;
  prenom_livreur: any;

  constructor(private firestore: AngularFirestore,
              private alertController: AlertController,
              private router: Router) {

    this.code_commande = localStorage.getItem('code_commande');
    this.vendeur_actif = localStorage.getItem('stock_vendeur');

    console.log(this.code_commande);

    // tslint:disable-next-line:max-line-length
    this.firestore.firestore.collection('commandes').where('statut', '==', 'en_livraison').where('vendeur_choisi', '==', this.vendeur_actif).where('codeSecret', '==', +this.code_commande).onSnapshot(
      (querySnapshot) => {
        // tslint:disable-next-line:prefer-const
        let livreur;
        querySnapshot.forEach((doc) => {
          this.produits.push({
            produit: doc.data().produit,
            prix: doc.data().amount,
            qty: doc.data().qty
          });
          console.log(doc.data().produit);
          console.log(doc.data().amount);
          console.log(doc.data().qty);
          this.total = doc.data().prix_total;
          this.numero_commande = doc.data().cmdCode;
          livreur = doc.data().email_livreur_choisi;
        });
        firestore.firestore.collection('livreur').where('email', '==',  livreur).onSnapshot(
          // tslint:disable-next-line:no-shadowed-variable
          (querySnapshot) => {
            querySnapshot.forEach((doc) => {
              console.log(doc.data().nom);
              this.nom_livreur = doc.data().nom;
              this.prenom_livreur = doc.data().prenom;
            });
          }
        );
      }
    );
  }

  ngOnInit() {}



  livrer() {
    // tslint:disable-next-line:max-line-length
    this.firestore.firestore.collection('commandes').where('statut', '==', 'en_livraison').where('vendeur_choisi', '==', this.vendeur_actif).where('codeSecret', '==', +this.code_commande).onSnapshot(
      (querySnapshot) => {
        querySnapshot.forEach((doc) => {
          this.firestore.collection('commandes').doc(doc.id).update({
            statut: 'remise'
          });
        });
      }
    );
    this.presentAlert();
  }


  async presentAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      message: 'vente reussi',
      buttons: ['ok']
    });

    await alert.present();
    this.router.navigate(['vendeur-tabs/accueil']);
  }
}
