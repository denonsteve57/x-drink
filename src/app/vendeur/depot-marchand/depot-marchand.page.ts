import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { AuthentificationService } from 'src/app/authentification/authentification.service';
import { Geolocation} from '@capacitor/core';

// tslint:disable-next-line:class-name
interface Nom_prods {
  art: string;
}
@Component({
  selector: 'app-depot-marchand',
  templateUrl: './depot-marchand.page.html',
  styleUrls: ['./depot-marchand.page.scss'],
})
export class DepotMarchandPage implements OnInit {
  // tslint:disable-next-line:variable-name
  nom_prods = [];
  dataStock =  {
    qty: '0',
    prix_unitaire: 0,
    unite: '',
    nom_prod: '',
    nom_depot: '',
    latitude: 0,
    longitude: 0,
    localisation_detail1: '',
    localisation_detail2: ''
  };
  dataDepot = {
    nom_depot: '',
    nom: '',
    email: '',
    telephone_gerant: '',
    ifu: '',
    adresse_livraison: '',
    password: '',
    password2: '',
    type: 'vendeur',
    latitude: 0,
    longitude: 0,
    localisation_detail1: '',
    localisation_detail2: ''
  };

  errorMessage: any;

  latitude: number;
  longitude: number;

  constructor(private router: Router,
              private authService: AuthentificationService,
              private firestore: AngularFirestore,
              public alertController: AlertController)
              {
                /*if (localStorage.getItem('nom_prods')){
                  this.nom_prods = JSON.parse(localStorage.getItem('nom_prods'));
                }*/
                this.nom_prods = JSON.parse(localStorage.getItem('nom_prods')) || [];
                console.log(this.nom_prods);

              }

  ngOnInit() {
  }

  soumettre() {
    console.log(this.dataDepot.localisation_detail1, this.dataDepot.localisation_detail2);
    if (this.dataDepot.password === this.dataDepot.password2) {
      const email = this.dataDepot.email;
      const password = this.dataDepot.password;
      this.authService.signUpUser(email, password).then(
        () => {
          localStorage.pseudo = this.dataDepot.nom;
          localStorage.email = this.dataDepot.email;
          localStorage.type = 'vendeur';
          localStorage.stock_vendeur = this.dataDepot.nom_depot;
          this.firestore.collection('depot_marchand').add(this.dataDepot);
          this.firestore.collection('user').add(this.dataDepot);
          this.nom_prods.forEach(element => {
            console.log(element.art);
            this.dataStock.nom_prod = element.art;
            this.dataStock.nom_depot = this.dataDepot.nom_depot;
            this.dataStock.localisation_detail1 = this.dataDepot.localisation_detail1;
            this.dataStock.localisation_detail2 = this.dataDepot.localisation_detail2;
            this.firestore.collection(this.dataDepot.nom_depot).doc(element.art).set(this.dataStock);
          });
            // identification du depot qui commande
          this.firestore.firestore.collection('depot_marchand').where('email', '==', email).onSnapshot(
            (querySnapshot) => {
             querySnapshot.forEach((doc) => {
                 localStorage.nom_dep = doc.data().nom_depot;
                 localStorage.latitude = doc.data().latitude;
                 localStorage.longitude = doc.data().longitude;
               });
           });
          this.router.navigate(['vendeur-tabs']);
        },
        (error) => {
          this.errorMessage = error;
        }
      );
    }
  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'IMPORTANT!',
      message: ' <strong>Avant de confirmer la géolocalisation vous devez être dans votre depot</strong>!!!',
      buttons: [
        {
          text: 'Non',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Confirmer position',
          handler: () => {
            console.log('Confirm Okay');
            this.getPosition();
          }
        }
      ]
    });

    await alert.present();
  }

  async getPosition() {
    const position = await Geolocation.getCurrentPosition();
    this.dataDepot.latitude = position.coords.latitude;
    this.dataDepot.longitude = position.coords.longitude;
    this.dataStock.latitude = position.coords.latitude;
    this.dataStock.longitude = position.coords.longitude;
    console.log(this.dataDepot.latitude, this.dataDepot.longitude);
  }

}
