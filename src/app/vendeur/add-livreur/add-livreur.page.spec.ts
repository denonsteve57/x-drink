import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddLivreurPage } from './add-livreur.page';

describe('AddLivreurPage', () => {
  let component: AddLivreurPage;
  let fixture: ComponentFixture<AddLivreurPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddLivreurPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddLivreurPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
