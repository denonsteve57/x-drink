import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Action } from 'rxjs/internal/scheduler/Action';

@Component({
  selector: 'app-vendeur-stock',
  templateUrl: './vendeur-stock.page.html',
  styleUrls: ['./vendeur-stock.page.scss'],
})
export class VendeurStockPage implements OnInit {

  dataStock =  {
    qty: '',
    prix_unitaire: '',
    unite: ''
  };

  categories = ['Braserie', 'PACK', 'Autre', 'Braserie2', 'PACK2', 'Autre2'];
  selectedItem = [];
  articles = [];
  option = '';
  stock: string;
  data: Observable <any[]>;
  nom: any;
  prix: [];
  // tslint:disable-next-line:variable-name
  nom_prod: any;
  val: any;
  constructor( private firestore: AngularFirestore,
               private router: Router,
               private storage: AngularFireStorage)
               {
                /* this.val = 'non';
                 this.articles = [];
                 this.getImageFirestore ();
                 this.stock = localStorage.getItem('stock_vendeur');*/
                }

  ngOnInit() {

    this.val = 'non';
    this.articles = [];
    this.getImageFirestore ();
    this.stock = localStorage.getItem('stock_vendeur');
  }




  getImageFirestore() {
    this.firestore.collection('sobebra').snapshotChanges()
    .subscribe( sob => {
      sob.forEach(sobx => {
        // tslint:disable-next-line:no-string-literal
        console.log ('image:' + sobx.payload.doc.data()['image'] );
        this.getArticleStorage(sobx);
        // tslint:disable-next-line:no-string-literal
        console.log(sobx.payload.doc.data()['nom_produit']);
        /*this.new(sobx);*/
      } );
    });

  }
 /* new(nom: any) {
    // tslint:disable-next-line:no-string-literal
    const nomRef = nom.payload.doc.data()['nom_produit'];
    this.firestore.collection(this.stock).doc(nomRef).snapshotChanges()
    .subscribe( () => {
      this.articles.push({
        // tslint:disable-next-line:no-string-literal
        amount: nom.payload.doc.data()['prix_unitaire']
      });
    });

  }*/

  getArticleStorageSob(image: any) {
    // tslint:disable-next-line:no-string-literal
    const imgRef = image.payload.doc.data()['image'];
    this.storage.ref(imgRef).getDownloadURL().subscribe(imgUrl => {
      console.log(imgUrl);
      this.articles.push({
        // tslint:disable-next-line:no-string-literal
        id: image.payload.doc.data()['id'],
        // tslint:disable-next-line:no-string-literal
        name: image.payload.doc.data()['nom_produit'],
        img: imgUrl,
        // tslint:disable-next-line:no-string-literal
        amount: image.payload.doc.data()['prix'],
      });
    }

    );
  }

  getArticleStorage(image: any) {
     // tslint:disable-next-line:no-string-literal
     const nomRef = image.payload.doc.data()['nom_produit'];
    // tslint:disable-next-line:no-string-literal
     const imgRef = image.payload.doc.data()['image'];
     this.storage.ref(imgRef).getDownloadURL().subscribe(imgUrl => {
      console.log(imgUrl);
      this.firestore.collection(this.stock).doc(nomRef).snapshotChanges()
      .subscribe( action => {
        this.articles.push({
          // tslint:disable-next-line:no-string-literal
          id: image.payload.doc.data()['id'],
          // tslint:disable-next-line:no-string-literal
          name: image.payload.doc.data()['nom_produit'],
          img: imgUrl,
          // tslint:disable-next-line:no-string-literal
          amount: action.payload.data()['prix_unitaire'],
          // tslint:disable-next-line:no-string-literal
          qty: action.payload.data()['qty'],
        });
      });
     /* this.articles.push({
        // tslint:disable-next-line:no-string-literal
        id: image.payload.doc.data()['id'],
        // tslint:disable-next-line:no-string-literal
        name: image.payload.doc.data()['nom_produit'],
        img: imgUrl,
        amount: this.prix,
      });*/
    }
    );

     // tslint:disable-next-line:no-string-literal
    /* const nomRef = image.payload.doc.data()['nom_produit'];
    // tslint:disable-next-line:no-string-literal
     console.log(image.payload.doc.data()['nom_produit']);
     this.firestore.collection(this.stock).doc(nomRef).snapshotChanges()
     .subscribe( action => {
       this.articles.push({
         // tslint:disable-next-line:no-string-literal
         amount: action.payload.data()['prix_unitaire'],
       });
       // tslint:disable-next-line:no-string-literal
       this.prix = action.payload.data()['prix_unitaire'];
       console.log(this.prix);
       });*/
  }

  mod(item) {
    this.val = 'oui';

  }
  isSelected(item){
    let find = false;
    this.selectedItem.forEach(element => {
      if (element === item){
        find = true;
      }
    });
    return find;
  }
  select(item){
    if (this.option !== ''){
      if (this.isSelected(item)){
        this.selectedItem.splice(this.selectedItem.indexOf(item), 1);
      }
      else{
        this.selectedItem.push(item);
      }
    }

    localStorage.setItem('produit-stock', JSON.stringify(this.selectedItem));
  }

  validate(){
    this.router.navigate(['vendeur-panier-stock']);
  }

  achat_hors_ligne(){
    console.log('achat');
    this.option = 'en-ligne';
    localStorage.option = 'achat';
  }
  vente_hors_ligne(){
    console.log('vente');
    this.option = 'hors-ligne';
    localStorage.option = 'vente';
  }

}
