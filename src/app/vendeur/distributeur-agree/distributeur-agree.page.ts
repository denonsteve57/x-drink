import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { ActionSheetController } from '@ionic/angular';
import { Geolocation} from '@capacitor/core';
import { AlertController } from '@ionic/angular';
import { AuthentificationService } from 'src/app/authentification/authentification.service';

@Component({
  selector: 'app-distributeur-agree',
  templateUrl: './distributeur-agree.page.html',
  styleUrls: ['./distributeur-agree.page.scss'],
})
export class DistributeurAgreePage implements OnInit {

   // tslint:disable-next-line:variable-name
   nom_prods = [];
   dataStock =  {
     qty: '0',
     prix_unitaire: 0,
     unite: '',
     nom_prod: '',
     nom_depot: '',
     latitude: 0,
     longitude: 0,
     localisation_detail1: '',
     localisation_detail2: ''
   };

  dataDistributeur = {
    type_distributeur: '',
    distributeur_agreer: '',
    email: '',
    password: '',
    password2: '',
    type: 'vendeur',
    secretCode: '',
    latitude: 0,
    longitude: 0,
    localisation_detail1: '',
    localisation_detail2: ''
  };

  // tslint:disable-next-line:variable-name
  type_distributeur: any;

  errorMessage: any;

  code: any;

  verificationCode = false;

  constructor(private router: Router,
              private authService: AuthentificationService,
              private firestore: AngularFirestore,
              public alertController: AlertController,
              public actionSheetController: ActionSheetController) {

                this.nom_prods = JSON.parse(localStorage.getItem('nom_prods')) || [];
                console.log(this.nom_prods);
              }

  ngOnInit() {
  }

  soumettre2() {
    // tslint:disable-next-line:max-line-length
    this.firestore.firestore.collection('code-confirmation-distributeur-agreer').where('nom', '==', this.dataDistributeur.distributeur_agreer).onSnapshot(
      (querySnapshot) => {
       querySnapshot.forEach((doc) => {
           this.code = doc.data().code;
         });
     });

    console.log(this.code);
    if (this.code === this.dataDistributeur.secretCode) {
      if (this.dataDistributeur.password === this.dataDistributeur.password2 ) {
        const email = this.dataDistributeur.email;
        const password = this.dataDistributeur.password;
        this.authService.signUpUser(email, password).then(
          () => {
            // localStorage.pseudo = this.dataDistributeur.nom;
            localStorage.email = this.dataDistributeur.email;
            localStorage.type = 'vendeur';
            localStorage.stock_vendeur = this.dataDistributeur.distributeur_agreer;
            this.firestore.collection('distributeur_agreer').add(this.dataDistributeur);
            this.firestore.collection('user').add(this.dataDistributeur);
            this.nom_prods.forEach(element => {
            console.log(element.art);
            this.dataStock.nom_prod = element.art;
            this.dataStock.nom_depot = this.dataDistributeur.distributeur_agreer;
            this.dataStock.localisation_detail1 = this.dataDistributeur.localisation_detail1;
            this.dataStock.localisation_detail2 = this.dataDistributeur.localisation_detail2;
            this.firestore.collection(this.dataDistributeur.distributeur_agreer).doc(element.art).set(this.dataStock);
          });
            this.firestore.firestore.collection('distributeur_agreer').where('email', '==', email).onSnapshot(
            (querySnapshot) => {
             querySnapshot.forEach((doc) => {
                 localStorage.nom_distributeur = doc.data().distributeur_agreer;
                 localStorage.latitude = doc.data().latitude;
                 localStorage.longitude = doc.data().longitude;
               });
           });
            this.router.navigate(['vendeur-tabs']);
          }, (error) => {
            this.errorMessage = error;
          }
        );
      }

     }else {
       this.verificationCode = true;
       // tslint:disable-next-line:prefer-const
       let message = 'code de verification incorrect! veuillez nous contacter';
     }
  }

  soumettre() {
    if (this.dataDistributeur.password === this.dataDistributeur.password2) {
      const email = this.dataDistributeur.email;
      const password = this.dataDistributeur.password;
      this.authService.signUpUser(email, password).then(
        () => {
          localStorage.pseudo = this.dataDistributeur.distributeur_agreer;
          localStorage.email = this.dataDistributeur.email;
          localStorage.type = 'vendeur';
          this.firestore.collection('distributeur_agree').add(this.dataDistributeur);
          this.firestore.collection('user').add(this.dataDistributeur);
          this.router.navigate(['vendeur-tabs']);
        },
        (error) => {
          this.errorMessage = error;
        }
      );
    }
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Types de distributeur',
      cssClass: 'my-custom-class',
      buttons: [{
        text: 'Distributeur agreer SOBEBRA',
        icon: 'trash',
        handler: () => {
          console.log('sobebra clicked');
          this.type_distributeur = 'sobebra';
        }
      }, {
        text: 'Distributeur LIBS',
        icon: 'share',
        handler: () => {
          console.log('libs clicked');
          this.type_distributeur = 'libs';
        }
      }, {
        text: 'Grossiste LIQUEURS ET CHAMPAGNE',
        icon: 'caret-forward-circle',
        handler: () => {
          console.log('liqueur et champagne clicked');
          this.type_distributeur = 'liqueur et champagne';
        }
      }, {
        text: 'Grossiste JUS ET AUTRES',
        icon: 'heart',
        handler: () => {
          console.log('jus et autres clicked');
          this.type_distributeur = 'jus et autres';
        }
      }, {
        text: 'Autres GROSSISTE',
        icon: 'heart',
        handler: () => {
          console.log('autres grossistes clicked');
          this.type_distributeur = 'atres grossistes';
        }
      },
         {
        text: 'cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'IMPORTANT!',
      message: ' <strong>Avant de confirmer la géolocalisation vous devez être dans votre depot principal</strong>!!!',
      buttons: [
        {
          text: 'Non',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Confirmer position',
          handler: () => {
            console.log('Confirm Okay');
            this.getPosition();
          }
        }
      ]
    });

    await alert.present();
  }

  async getPosition() {
    const position = await Geolocation.getCurrentPosition();
    this.dataDistributeur.latitude = position.coords.latitude;
    this.dataDistributeur.longitude = position.coords.longitude;
   // this.dataStock.latitude = position.coords.latitude;
   // this.dataStock.longitude = position.coords.longitude;
    console.log(this.dataDistributeur.latitude, this.dataDistributeur.longitude);
  }

}
