import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';

@Component({
  selector: 'app-code-de-vente',
  templateUrl: './code-de-vente.page.html',
  styleUrls: ['./code-de-vente.page.scss'],
})
export class CodeDeVentePage implements OnInit {
  // tslint:disable-next-line:variable-name
  vendeur_actif: string;
  // tslint:disable-next-line:variable-name
  code_commandes_doublons = [];
  // tslint:disable-next-line:variable-name
  code_commandes: any[];

  code1: number;
  code2: number;
  code3: number;
  code4: number;
  code5: number;
  code6: number;
  // tslint:disable-next-line:variable-name
  code_entrer: number;
  message: any;


  constructor(private firestore: AngularFirestore,
              private router: Router) { }

  ngOnInit() {
  }

  Servir() {

    console.log(this.code1,this.code2,this.code3,this.code4,this.code5,this.code6);
    this.code_entrer = this.code1 + this.code2 + this.code3 + this.code4 + this.code5 + this.code6;
    console.log(+this.code_entrer);

    this.vendeur_actif = localStorage.getItem('stock_vendeur');

    // tslint:disable-next-line:max-line-length
    this.firestore.firestore.collection('commandes').where('statut', '==', 'en_livraison').where('vendeur_choisi', '==', this.vendeur_actif).onSnapshot(
      (querySnapchot) => {
        this.code_commandes_doublons = [];
        querySnapchot.forEach((doc) => {
          this.code_commandes_doublons.push(doc.data().codeSecret);
        });
        this.code_commandes = Array.from(new Set (this.code_commandes_doublons));
        console.log(this.code_commandes);
        console.log(this.code_entrer);
        // tslint:disable-next-line:variable-name
        this.code_commandes.forEach(code_commandesx => {
        if ( +this.code_entrer === code_commandesx) {
         console.log('bien');
         localStorage.code_commande = code_commandesx;
         this.router.navigate(['vendeur-panier']);
       }else {
         this.message = 'ce code ne correspond à aucune de vos commandes';
       }


    });

      }
    );


  }

}
