import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { ModifierProduitPanierPage } from 'src/app/modals/modifier-produit-panier/modifier-produit-panier.page';

@Component({
  selector: 'app-vendeur-panier-stock',
  templateUrl: './vendeur-panier-stock.page.html',
  styleUrls: ['./vendeur-panier-stock.page.scss'],
})
export class VendeurPanierStockPage implements OnInit {

  // tslint:disable-next-line:variable-name
  produit_panier = [];
  cartStock = [];
  // tslint:disable-next-line:variable-name
  nom_prod: any;
  constructor(private modalController: ModalController,
              private router: Router,
              private firestore: AngularFirestore) { }

  ngOnInit() {
    if (localStorage.getItem('produit-stock')){
      this.produit_panier = JSON.parse(localStorage.getItem('produit-stock'));
    }
  }

  openModal(item){
    this.presentModal();
    this.cartStock.push(item);
    this.nom_prod = item.name;
    localStorage.nom_prod = item.name;
    console.log(this.nom_prod);
    localStorage.setItem('stockref', JSON.stringify(this.cartStock));
    // tslint:disable-next-line:prefer-const
    let exist = false;

    this.cartStock.forEach(element => {
      if (element.id === item.id){
        exist = true;
        this.cartStock.pop();
      }
    });
  }

  delete(item){
    this.produit_panier.forEach((element, i) => {
      if (element.id === item.id){
        console.log('i :>> ', i);
        this.produit_panier.splice(i, 1);
      }
    });
  }
  async presentModal() {
    const modal = await this.modalController.create({
      component: ModifierProduitPanierPage,
      cssClass: 'livraison-modal',
      componentProps: {
      }
    });
    await modal.present();
    const { data } = await modal.onWillDismiss();
    console.log(data);
  }

  actualiser() {
    this.router.navigateByUrl('vendeur-tabs/stock');
  }

}
