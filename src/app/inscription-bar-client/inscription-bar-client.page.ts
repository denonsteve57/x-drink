import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Geolocation} from '@capacitor/core';
import { AuthentificationService } from '../authentification/authentification.service';

@Component({
  selector: 'app-inscription-bar-client',
  templateUrl: './inscription-bar-client.page.html',
  styleUrls: ['./inscription-bar-client.page.scss'],
})
export class InscriptionBarClientPage implements OnInit {
  dataBar = {
    nom_Bar: '',
    nom: '',
    telephone_gerant: '',
    email: '',
    ifu: '',
    adresse_livraison: '',
    password: '',
    password2: '',
    type: 'client',
    latitude: 0,
    longitude: 0
  };
  errorMessage: any;

  constructor(
    private firestore: AngularFirestore,
    private authService: AuthentificationService,
    private router: Router,
    public alertController: AlertController
  ) {}

  ngOnInit() {
  }

  soumettre() {
    if (this.dataBar.password === this.dataBar.password2){
      const email = this.dataBar.email;
      const password = this.dataBar.password;
      this.authService.signUpUser(email, password).then(
        () => {
          localStorage.pseudo = this.dataBar.nom;
          localStorage.email = this.dataBar.email;
          localStorage.type = 'client';
          this.firestore.collection('bar-buvette').add(this.dataBar);
          this.firestore.collection('user').add(this.dataBar);
          localStorage.latitude = this.dataBar.latitude;
          localStorage.longitude = this.dataBar.longitude;
          localStorage.nom_bar = this.dataBar.nom_Bar;
          this.router.navigate(['tabs']);
        },
        (error) => {
         this.errorMessage = error;
        }
      );
    }
  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'IMPORTANT!',
      message: ' <strong>Avant de confirmer la géolocalisation vous devez être dans votre Bar/Buvette</strong>!!!',
      buttons: [
        {
          text: 'Non',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Confirmer position',
          handler: () => {
            console.log('Confirm Okay');
            this.getPosition();
          }
        }
      ]
    });

    await alert.present();
  }

  async getPosition() {
    const position = await Geolocation.getCurrentPosition();
    this.dataBar.latitude = position.coords.latitude;
    this.dataBar.longitude = position.coords.longitude;
    console.log(this.dataBar.latitude, this.dataBar.longitude);
  }



}
