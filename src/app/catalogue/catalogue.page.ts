import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.page.html',
  styleUrls: ['./catalogue.page.scss'],
})
export class CataloguePage  {
  sobebra: Observable<any[]>;
  articleSob = [];
  articleLiq = [];
  categorie = 'tout';
  // tslint:disable-next-line:variable-name
  nom_prod = [];

  
  categories = [
    {
      id: 1,
      name: 'Liqueur',
      img: '/assets/illustration/liqueurs1.png',
      qty: 0,
      amount: 500
    },
    {
      id: 2,
      name: 'Jus de fruit',
      img: '/assets/illustration/jus-de-fruits3.png',
      qty:0,
      amount:250
    },
    {
      id:3,
      name:"Canettes",
      img:"/assets/illustration/cannettes1.png",
      qty:0,
      amount:250
    },
    {
      id:4,
      name: 'Sobebra',
      img:"/assets/illustration/brasserie3.png",
      qty:0,
      amount:250
    }
  ]
  selectedCat={
    id:1,
    name:"Liqueur",
    img:"/assets/illustration/liqueurs1.png",
    qty:0,
    amount:250
  }

  categorieSelected: any;

  produits = [
    {
      id: 0,
      img: 'https://picsum.photos/200',
      name: 'Nom du produit',
      qty: 0,
      amount: 250
    }
  ];


  carts = [];

  constructor(private activatedRoute: ActivatedRoute, private router: Router,
              private toastController: ToastController,
              public firestore: AngularFirestore,
              private storage: AngularFireStorage)
              {
                // this.sobebra = this.firestore.collection('sobebra').valueChanges();
                this.getImageFirestoreSob();
                this.getImageFirestoreLiq();
               }

 /* ngOnInit() {
    [1,2,3,4,5,6,7,8,9].forEach(element => {
      this.produits.push(
        {
          id:element,
          img: "https://picsum.photos/200",
          name:"Nom du produit "+ element,
          qty:0,
          amount:250
        }
      )
    });
  }*/
  ionViewWillEnter(){
    // this.activatedRoute.queryParams.subscribe(params => {
    //   this.cat = this.router.getCurrentNavigation().extras.state.cat;
    //   console.log('this.cat :>> ', this.cat);
    // });
   /* this.cat = JSON.parse(localStorage.getItem('categorie'));
    if (this.cat){
      console.log('hello');
      this.selectedCat = this.cat;
    }
    console.log('this.cat :>> ', this.cat);*/
  }

  getImageFirestoreSob() {
    this.firestore.collection('sobebra').snapshotChanges()
    .subscribe( sob => {
      sob.forEach(sobx => {
        // tslint:disable-next-line:no-string-literal
        console.log ('image:' + sobx.payload.doc.data()['image'] );
        // tslint:disable-next-line:no-string-literal
        this.nom_prod.push({
          // tslint:disable-next-line:no-string-literal
          art: sobx.payload.doc.data()['nom_produit'],
        } );
        this.getArticleStorageSob(sobx);
      } );
      localStorage.setItem('nom_prods', JSON.stringify(this.nom_prod));
      console.log(this.nom_prod);
    });
  }

  getImageFirestoreLiq() {
    this.firestore.collection('liqueur').snapshotChanges()
    .subscribe( liq => {
      liq.forEach(liqx => {
        // tslint:disable-next-line:no-string-literal
        console.log ('image:' + liqx.payload.doc.data()['image'] );
        this.getArticleStorageLiq(liqx);
      } );
    });
  }

  getArticleStorageSob(image: any) {
    // tslint:disable-next-line:no-string-literal
    const imgRef = image.payload.doc.data()['image'];
    this.storage.ref(imgRef).getDownloadURL().subscribe(imgUrl => {
      console.log(imgUrl);
      this.articleSob.push({
        // tslint:disable-next-line:no-string-literal
        id: image.payload.doc.data()['id'],
        // tslint:disable-next-line:no-string-literal
        name: image.payload.doc.data()['nom_produit'],
        img: imgUrl,
      });
      // tslint:disable-next-line:no-string-literal
      console.log(image.payload.doc.data()['nom_produit']);
    }
    );
  }

  getArticleStorageLiq(image: any) {
     // tslint:disable-next-line:no-string-literal
     const imgRef = image.payload.doc.data()['image'];
     this.storage.ref(imgRef).getDownloadURL().subscribe(imgUrl => {
       console.log(imgUrl);
       this.articleLiq.push({
         // tslint:disable-next-line:no-string-literal
         id: image.payload.doc.data()['id'],
         // tslint:disable-next-line:no-string-literal
         name: image.payload.doc.data()['nom_produit'],
         img: imgUrl,
       });
     }
     );
  }

  voir() {
    console.log(this.categorieSelected);
    // tslint:disable-next-line:prefer-const
    if (this.categorieSelected === 'Sobebra') {
      this.categorie = 'sobebra';
    }
    if (this.categorieSelected === 'Liqueur') {
      this.categorie = 'liqueur';
    }


  }

  add(item){
    let exist = false;

    this.carts.forEach(element => {
      if (element.id === item.id){
        exist = true;
        console.log('eist');
        this.presentToast('deja');
      }
      console.log(element.name);

    });
    console.log(exist);
    if (exist === false){
      this.carts.push(item);
      this.presentToast('produit ajouter au panier ');
    }
    else{
     // this.presentToast();
    }
    localStorage.setItem('cart', JSON.stringify(this.carts));
  }

  async presentToast(message = '') {
    const toast = await this.toastController.create({
      message,
      duration: 2000
    });
    toast.present();
  }

  /*addToCart(): void {
   localStorage.article = this.articles;
  }*/

}
